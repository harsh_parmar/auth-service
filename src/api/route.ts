import { Routes } from 'nest-router';
import { AuthenticationModule } from '../core/authentication.module';
export const routes: Routes = [
  {
    path: '/v1',
    children: [AuthenticationModule],
  },
];