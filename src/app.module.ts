import {  Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import { RouterModule } from 'nest-router';
import { routes } from './api/route';
import { DatabaseModule } from './database/database.module';
import { AuthenticationModule } from './core/authentication.module';
import { Logger } from './logger/logger.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        POSTGRES_HOST: Joi.string().required(),
        POSTGRES_PORT: Joi.number().required(),
        POSTGRES_USER: Joi.string().required(),
        POSTGRES_PASSWORD: Joi.string().required(),
        POSTGRES_DB: Joi.string().required(),
        JWT_SECRET: Joi.string().required(),
        JWT_EXPIRATION_TIME: Joi.string().required(),
        PORT: Joi.number(),
      })
    }),
    DatabaseModule,
    AuthenticationModule,
    RouterModule.forRoutes(routes),
    Logger
  ],
  controllers: [],
  providers: [],
})
export class AppModule {
}

