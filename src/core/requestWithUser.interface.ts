import { Request } from 'express';
import User from './dto/user.dto';

interface RequestWithUser extends Request {
  user: User
}

export default RequestWithUser;