import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import RegisterDto from './dto/register.dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import TokenPayload from './tokenPayload.interface';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import PostgresErrorCode from 'src/errors/postgresErrorCode.enum';

@Injectable()
export class AuthenticationService {
  private readonly logger = new Logger('AuthenticationService');
  private readonly userClient = ClientProxyFactory.create({
    transport: Transport.TCP,
    options: {
      host: 'localhost',
      port: 4001
    }
  });
  constructor(
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService
  ) {

  }

  public async register(registrationData: RegisterDto) {
    const hashedPassword = await bcrypt.hash(registrationData.password, 10);
    try {
      return await this.userClient.send('user.create', {
        ...registrationData,
        password: hashedPassword
      }).toPromise();
    } catch (error) {
      if (error?.code === PostgresErrorCode.UniqueViolation) {
        throw new HttpException('User with that email already exists', HttpStatus.BAD_REQUEST);
      }
      throw new HttpException('Something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public getCookieWithJwtToken(userId: number, role: string) {
    const payload: TokenPayload = { userId, role };
    return this.jwtService.sign(payload);
  }

  public getCookieForLogOut() {
    return `Authentication=; HttpOnly; Path=/; Max-Age=0`;
  }

  public async getAuthenticatedUser(email: string, plainTextPassword: string) {
    try {

      const user = await this.userClient.send('getUserByEmail', email).toPromise();
      this.logger.log('User created', JSON.stringify(user));

      await this.verifyPassword(plainTextPassword, user.password);
      return user;
    } catch (error) {
      throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST);
    }
  }

  private async verifyPassword(plainTextPassword: string, hashedPassword: string) {
    const isPasswordMatching = await bcrypt.compare(
      plainTextPassword,
      hashedPassword
    );

    if (!isPasswordMatching) {
      throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST);
    }
  }
}
