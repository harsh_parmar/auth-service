import { Body, Req, Controller, HttpCode, Post, UseGuards, Res, Get } from '@nestjs/common';
import { Response } from 'express';
import { AuthenticationService } from './authentication.service';
import RegisterDto from './dto/register.dto';
import RequestWithUser from './requestWithUser.interface';
import { LocalAuthenticationGuard } from './localAuthentication.guard';
import JwtAuthenticationGuard from './jwt-authentication.guard';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { PinoLogger } from 'nestjs-pino';

@ApiTags('auth')
@Controller('auth')
export class Authentication {
  constructor(
    private readonly authenticationService: AuthenticationService,
    private readonly logger: PinoLogger
  ) {
    logger.setContext(Authentication.name)
  }

  @Post('register')
  async register(@Body() registrationData: RegisterDto) {
    return this.authenticationService.register(registrationData);
  }

  @HttpCode(200)
  @UseGuards(LocalAuthenticationGuard)
  @ApiProperty({
    
  })
  @Post('login')
  async logIn(@Req() request: RequestWithUser, @Res() response: Response) {
    const {user} = request;
    const token = this.authenticationService.getCookieWithJwtToken(user.id, user.role);
    user.password = undefined;
    return response.send({
      token,
      user
    });
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post('logout')
  async logOut(@Res() response: Response) {
    response.setHeader('Set-Cookie', this.authenticationService.getCookieForLogOut());
    return response.sendStatus(200);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get()
  authenticate(@Req() request: RequestWithUser) {
    const user = request.user;
    user.password = undefined;
    return user;
  }
}
