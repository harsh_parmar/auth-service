
class User {
  public id?: number;
  public email: string;
  public name: string;
  public password: string;
  public role: string;
}

export default User;