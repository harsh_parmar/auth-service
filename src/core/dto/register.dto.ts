import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from 'class-validator';

export class RegisterDto {
  
  @ApiProperty({
    description: "Email address",
    example: 'sa@mailinator.com'
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;
  
  @ApiProperty({
    description: "firstname",
    example: 'John'
  })
  firstName: string;
  
  @ApiProperty({
    example: 'Doe'
  })
  lastName: string;
  
  @ApiProperty({
    example: 'Abcd@123'
  })
  password: string;
}

export default RegisterDto;