import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';

@Module({
  imports: [
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
        useFactory: async (configService: ConfigService) => {
          const level = configService.get('LOG_LEVEL') ? configService.get('LOG_LEVEL'): 'info';
          return ({
            pinoHttp:{
              level
            }
          })
        }
      })
  ],
})
export class Logger {}
